import Foundation

enum Direction: Int {
    case right
    case left
    case up
    case down
}
