import Foundation

protocol NumberCellDelegate {
    
    func onCellCreated(at index: Int)
    func onCellMoved(from oldIndex: Int, to newIndex: Int)
    func onNumberUp(at index: Int)
    func onRemove(from oldIndex: Int, to newIndex: Int)
}
